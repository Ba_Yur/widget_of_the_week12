import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget of the week 12'),
      ),
      body: Column(
        children: [
          Placeholder(
            fallbackHeight: 200,
          ),
          isTapped? Tooltip(
            message: 'LimitedBox in ConstrainedBox',
            child: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 150, maxWidth: 150),
              child: LimitedBox(
                maxHeight: 200,
                maxWidth: 200,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Image.network('https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg'),
                ),
              ),
            ),
          ) : Tooltip(
            message: 'LimitedBox without ConstrainedBox, yes, and elephant in FittedBox',
            child: LimitedBox(
              maxHeight: 200,
              maxWidth: 200,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image.network('https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg'),
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              setState(() {
                isTapped = !isTapped;
              });
            },
            child: Text('Tap me'),
          ),
        ],
      ),
    );
  }
}
